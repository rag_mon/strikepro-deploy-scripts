# Git hooks bash скрипты для деплоя проектов Strikepro

* **strikepro-backend-post-receive** - strikepro backend (catalog; api: partner, dashboard);
* **strikepro-frontend-dashboard-post-receive** - dashboard frontend;
* **strikepro-frontend-partner-post-receive** - partner frontend;
* **strikepro-shop-post-receive** - shop backend;

## Установка
1. Инициализируем соответствующие GIT Bare репозитории на сервере.
К примеру: `mkdir -p ~/repos/strikepro-backend.git && cd ~/repos/strikepro-backend.git && git init --bare`.
2. Копируем `post-receive` индивидуально в соответствующий GIT Bare репозиторий.
К примеру: `cp ./strikepro-backend-post-receive /server/path/to/the/git-bare-name.git/hooks/post-receive`.
